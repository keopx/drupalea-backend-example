<?php

namespace Drupal\drupalea_backend\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a Backend test form.
 */
class BackendTestSearchForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupalea_backend_test_search';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->getRequest()->query->has('search')) {
      $search_default = $this->getRequest()->query->get('search');
    }
    if (isset($search_default) && !empty($search_default)) {
      $form['title'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Searched item: @search', ['@search' => $search_default]),
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
      ];
    }

    $form['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#default_value' => $search_default ?? '',
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'title' => $this->t('Search by city'),
      ],
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value'  => 'Reset',
      '#attributes' => [
        'title' => $this->t('Reset search result'),
      ],
      '#submit' => ['::reset'],
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * Form submission handler for the 'Return to' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function reset(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('drupalea_backend.search_test');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('search')) < 3) {
      $form_state->setErrorByName('search', $this->t('Message should be at least 3 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('drupalea_backend.search_test', [], [
      'query' => ['search' => $form_state->getValue('search')],
    ]);
  }

}
