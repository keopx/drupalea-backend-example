<?php

namespace Drupal\drupalea_backend\Controller;

use Drupal\drupalea_backend\Form\BackendTestSearchForm;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Backend test routes.
 */
class BackendTestController extends ControllerBase {

  /**
   * Items per page.
   *
   * @var int
   */
  const ITEMS_PER_PAGE = 5;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * A guzzle http client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * Constructs a new BackendTestSearchForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \GuzzleHttp\ClientInterface $client
   *   A guzzle http client instance.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, ClientInterface $client, PagerManagerInterface $pager_manager, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->configFactory = $config_factory;
    $this->requestStack = $request_stack;
    $this->client = $client;
    $this->pagerManager = $pager_manager;
    $this->loggerChannelFactory = $logger_channel_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('pager.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $build['search_form'] = $this->formBuilder()->getForm(BackendTestSearchForm::class);

    if ($this->requestStack->getCurrentRequest()->query->has('search')) {
      $this->getTable($build, $this->requestStack->getCurrentRequest()->query->get('search'));
    }

    return $build;
  }

  /**
   * Get the table.
   *
   * @param array $build
   *   Build array render items.
   * @param string $search
   *   Search string.
   */
  protected function getTable(array &$build, $search = '') {
    if (!empty($search)) {
      $header = [
        $this->t('Country'),
        $this->t('Name'),
      ];

      $rows = $this->getRows($search);

      if (!empty($rows)) {
        $build['container']['result'] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
          '#empty' => '',
        ];
        $build['container']['pager'] = [
          '#type' => 'pager',
          '#weight' => 10,
        ];
      }
      else {
        $build['container']['pager'] = [
          '#type' => 'markup',
          '#markup' => $this->t('No results for @name', ['@name' => $search]),
        ];
      }
    }
  }

  /**
   * Get data rows.
   *
   * @param string $key
   *   String to search.
   *
   * @return array
   *   List of items.
   */
  protected function getRows(string $key): array {
    $rows = [];
    $url = $this->configFactory->get('drupalea_backend.settings')->get('endpoint');
    $query_url = $url . '?name=' . $key;
    try {
      $request = $this->client->get($query_url);
      $data = Json::decode($request->getBody());
      $rows = [];
      if (!empty($data)) {
        foreach ($data as $item) {
          $rows[] = [
            'country' => $item['country'] ?? '',
            'name' => $item['name'] ?? '',
          ];
        }
      }
    }
    catch (\Exception $e) {
      $this->loggerChannelFactory->get('drupalea_backend')->error($e->getMessage());
    }
    return $rows;
  }

}
