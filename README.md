# Drupalea Backend Example

## Introducción

Modulo creado para el workshop Introducción al desarrollo de módulos en Drupal 10
de la DrupalCamp Spain 2023.

## Código

Este modulo es un código de ejemplo de como crear un formulario básico para
consumir recursos de una API externa.

- Creación de modulo.
- Creación de setting form.
- Creación de form.
- Creación de un controlador.
- Creación de un menú option.
- Creación de un permiso.
- Creación de un settings default.
- Inyección de dependencias.
